# Notebook for Machine Learning

This ZApp contains the official GATK docker image.



## Running your own script

By modifying the `command` parameter in the JSON file you can tell Zoe to run your own script instead of the notebook.

In this ZApp the default command is:

    "command": "jupyter lab --no-browser --NotebookApp.token='' --allow-root --ip=0.0.0.0"

If you change the JSON and write:

    "command": "/mnt/workspace/myscript.sh"

Zoe will run myscript.sh instead of running the Jupyter notebook. In this way you can:

 * transform an interactive notebook ZApp into a batch one, with exactly the same libraries and environment
 * perform additional setup before starting the notebook. In this case you will have to add the jupyter lab command defined above at the end of your script.

